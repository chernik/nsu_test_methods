from openjdk:8

WORKDIR /wdr
RUN curl http://mirror.linux-ia64.org/apache/tomcat/tomcat-9/v9.0.37/bin/apache-tomcat-9.0.37.tar.gz > tomcat.tar.gz
RUN curl -L https://bitbucket.org/tzolotuhin/test-methods/downloads/tm-1.0.1.zip > app.zip
RUN tar -xf tomcat.tar.gz && mv apache-tomcat-9.0.37 tomcat
RUN unzip app.zip && mv tm-1.0.0 app
RUN cp app/tm-*.war tomcat/webapps/

RUN apt update
RUN apt install -y lsb-release

RUN wget http://repo.mysql.com/mysql-apt-config_0.8.13-1_all.deb
RUN DEBIAN_FRONTEND=noninteractive dpkg -i mysql-apt-config_0.8.13-1_all.deb
RUN apt update
RUN DEBIAN_FRONTEND=noninteractive apt install -y mysql-server

RUN sed -i 's/IDENTIFIED BY/IDENTIFIED WITH mysql_native_password BY/g' app/scripts/db_create.sql

COPY run.sh .
CMD ["sh", "./run.sh"]
