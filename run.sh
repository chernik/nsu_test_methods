#/usr/bin/sh

/usr/bin/mysqld_safe &
sucs=1
while [ "$sucs" != "0" ]
do
	sleep 1
	mysql -e "\?"
	sucs=$?
done

mysql < app/scripts/db_create.sql

tomcat/bin/startup.sh

sleep infinity
